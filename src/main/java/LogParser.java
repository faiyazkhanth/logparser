import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

/**
 * @author faiyaz.khan
 * @since ১৮/৭/২১
 */
public class LogParser {
    public static void main(String[] args) {

        String fileName = args[0];
        ArrayList<HourLog> entries = new ArrayList<>();
        boolean sorted = false;
        if(args.length == 2 && args[1].equals("--sort")){
            sorted = true;
        }

        String folderPath = "/home/faiyaz.khan/Assignments/LogParser/";
        String filePath = folderPath + fileName;
        ArrayList<String> lines = Helper.readLogFile(filePath);

        for (String line : lines) {
            int responseTime = Helper.findResponseTime(line);
            int getCnt = Helper.countGet(line);
            int postCnt = Helper.countPost(line);
            String url = Helper.findUrl(line);
            String startTime = Helper.formatTime(line)[0];
            String endTime = Helper.formatTime(line)[1];

            int idx = Helper.findHourLog(entries, startTime);
            if(idx != -1){
                HourLog entry = entries.get(idx);
                entry.setGetCnt(entry.getGetCnt()+getCnt);
                entry.setPostCnt(entry.getPostCnt()+postCnt);
                entry.setTotalResponseTime(entry.getTotalResponseTime()+responseTime);
                entry.setTotalCnt(entry.getTotalCnt()+getCnt+postCnt);
                HashSet<String> urls = entry.getUniqueUrls();
                urls.add(url);
                entry.setUniqueUrls(urls);
            }
            else{
                HourLog entry = new HourLog(startTime, endTime, getCnt, postCnt, getCnt+postCnt, responseTime, url);
                entries.add(entry);
            }

        }

        if(sorted){
            System.out.println("sorted called");
            TotalGPSort totalGPSort = new TotalGPSort();
            Collections.sort(entries, totalGPSort);
        }

        CommandLineTable commandLineTable = new CommandLineTable();
        commandLineTable.setShowVerticalLines(true);
        commandLineTable.setHeaders("Time", "GET/POST Count", "Unique URI Count", "Total Response Time");
        for(HourLog entry:entries){
            String time=entry.getStartTime() + " - " + entry.getEndTime();
            String count = entry.getGetCnt()+"/"+entry.getPostCnt();
            String countUniqueUrl = ""+entry.getUniqueUrls().size();
            String totalResponseTime = ""+entry.getTotalResponseTime();
            commandLineTable.addRow(time, count, countUniqueUrl, totalResponseTime);
        }
        commandLineTable.print();

    }
}
