import java.util.Comparator;

/**
 * @author faiyaz.khan
 * @since ১৯/৭/২১
 */
public class TotalGPSort implements Comparator<HourLog> {
    public int compare(HourLog one, HourLog two){
        return Integer.toString(two.getTotalCnt()).compareTo(Integer.toString(one.getTotalCnt()));
    }
}
