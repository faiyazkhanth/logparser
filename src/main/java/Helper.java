import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author faiyaz.khan
 * @since ১৯/৭/২১
 */
public class Helper{

    static ArrayList<String> readLogFile(String filePath){
        ArrayList<String> lines = new ArrayList<>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(filePath));
            String line = br.readLine();
            while (line != null) {
                lines.add(line);
                line = br.readLine();
            }
            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lines;
    }

    static int findResponseTime(String line){
        int responseTime = 0;

        Pattern patternResponseTime = Pattern.compile("time=([0-9]+)ms");
        Matcher matcherResponseTime = patternResponseTime.matcher(line);
        if(matcherResponseTime.find()){
            responseTime = Integer.parseInt(matcherResponseTime.group(1));
        }
        return responseTime;
    }

    static int countGet(String line){
        int getCnt = 0;
        Pattern patternGet = Pattern.compile(" G,");
        Matcher matcherGet = patternGet.matcher(line);
        if(matcherGet.find()){
            getCnt = 1;
        }
        return getCnt;
    }

    static int countPost(String line){
        int postCnt = 0;
        Pattern patternGet = Pattern.compile(" P,");
        Matcher matcherGet = patternGet.matcher(line);
        if(matcherGet.find()){
            postCnt = 1;
        }
        return postCnt;
    }

    static String findUrl(String line){
        String url = null;
        Pattern patternUrl = Pattern.compile("URI=\\[(.*?)\\]");
        Matcher matcherUrl = patternUrl.matcher(line);
        if(matcherUrl.find()){
            url = matcherUrl.group(1);
        }
        return url;
    }

    static String[] formatTime(String line){

        String startTime = "", endTime = "";
        String lineTime = line.split(" ")[1];
        String lineTimeHour = lineTime.split(":")[0];

        if(Integer.parseInt(lineTimeHour) > 12){
            startTime = "" + (Integer.parseInt(lineTimeHour) - 12);
            if(startTime.equals("12")){
                startTime += ".00 am";
                endTime = "1.00 am";
            }
            else if(startTime.equals("11")){
                startTime += ".00pm";
                endTime = "12.00am";
            }
            else{
                endTime = "" + (Integer.parseInt(startTime)+1) + ".00pm";
                startTime += ".00pm";
            }
        }
        else{
            startTime = "" + Integer.parseInt(lineTimeHour);
            if(startTime.equals("00")){
                startTime = "1.00am";
                endTime = "2.00am";
            }
            else if(startTime.equals("11")){
                startTime = "11.00am";
                endTime = "12.00pm";
            }
            else{
                endTime = "" + (Integer.parseInt(startTime)+1) + ".00am";
                startTime += ".00am";
            }
        }
        String[] times = {startTime, endTime};
        return times;
    }

    static int findHourLog(ArrayList<HourLog> entries, String startTime){
        int idx = -1;
        for(HourLog entry: entries){
            if(entry.getStartTime().equals(startTime)){
                idx = entries.indexOf(entry);
                break;
            }
        }
        return idx;
    }
}
