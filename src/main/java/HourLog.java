import java.util.HashSet;

/**
 * @author faiyaz.khan
 * @since ১৯/৭/২১
 */
public class HourLog {
    private String startTime, endTime;
    private int getCnt, postCnt, totalCnt, totalResponseTime;
    private HashSet<String> uniqueUrls;

    public HourLog(String startTime, String endTime, int getCnt, int postCnt, int totalCnt, int totalResponseTime, String url) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.getCnt = getCnt;
        this.postCnt = postCnt;
        this.totalCnt = totalCnt;
        this.totalResponseTime = totalResponseTime;
        this.uniqueUrls = new HashSet<>();
        uniqueUrls.add(url);
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public int getGetCnt() {
        return getCnt;
    }

    public void setGetCnt(int getCnt) {
        this.getCnt = getCnt;
    }

    public int getPostCnt() {
        return postCnt;
    }

    public void setPostCnt(int postCnt) {
        this.postCnt = postCnt;
    }

    public int getTotalCnt() {
        return totalCnt;
    }

    public void setTotalCnt(int totalCnt) {
        this.totalCnt = totalCnt;
    }

    public int getTotalResponseTime() {
        return totalResponseTime;
    }

    public void setTotalResponseTime(int totalResponseTime) {
        this.totalResponseTime = totalResponseTime;
    }

    public HashSet<String> getUniqueUrls() {
        return uniqueUrls;
    }

    public void setUniqueUrls(HashSet<String> uniqueUrls) {
        this.uniqueUrls = uniqueUrls;
    }

}
